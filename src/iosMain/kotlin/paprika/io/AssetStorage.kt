package paprika.io

import paprika.canvas.sources.ImageSource
import paprika.io.sources.IOSImageSource
import paprika.io.task.Task
import paprika.io.task.task
import platform.Foundation.*
import platform.UIKit.UIImage

class AssetStorage(val base: String = "assets") : ReadableStorage {
    override fun support(id: String): Boolean =
        StorageTypes.ASSETS.support(id)

    override fun readText(id: String): Task<String> =  task {

        val path = Path.removeProtocol(id)
        val dir = Path.join(base, Path.dirName(path))
        val file = Path.baseName(path)
        val ressourcePath = NSBundle.mainBundle.pathForResource(file, null, dir) ?: NOT_FOUND(path)
        val url = NSURL(fileURLWithPath = ressourcePath)
//        val data = NSData.dataWithContentsOfURL(url) ?: throw Exception("unable to load data $path")

        val fm = NSFileManager()
        val fullPath = Path.join(base, path)
        val exists = fm.fileExistsAtPath(url.path!!)

        if (exists) {
            val c = fm.contentsAtPath(url.path!!)
            val cString = NSString.create(c!!, NSUTF8StringEncoding)
            it.resolve(cString as String)
        }
        else
            it.reject(Exception("Text file not found $path"))
    }

    override fun has(id: String): Boolean =
        StorageTypes.ASSETS.support(id)

    override fun readImage(id: String): Task<ImageSource> =
        task {
            val path = Path.removeProtocol(id)
            val dir = Path.join(base, Path.dirName(path))
            val file = Path.baseName(path)
            val fileNoExt = Path.removeExtension(file)
            val ext = Path.extension(path)

            val resourcePath = NSBundle.mainBundle.pathForResource(fileNoExt, ext, dir)
                ?: throw Exception("unable to load $path")
            val image = UIImage(contentsOfFile = resourcePath)

            it.resolve(IOSImageSource(image))
        }

}

