package paprika.io

import android.content.res.AssetManager
import android.graphics.BitmapFactory
import paprika.canvas.sources.ImageSource
import paprika.io.sources.AndroidImageSource
import paprika.io.task.Task
import paprika.io.task.task

class AssetStorage(private val assetManager: AssetManager) : ReadableStorage {
    override fun support(id: String): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun readText(id: String): Task<String> = task {
        val path = Path.removeProtocol(id)
        val result = assetManager.open(path).bufferedReader().use { it.readText() }
        it.resolve(result)
    }

    override fun has(id: String): Boolean =
        StorageTypes.ASSETS.support(id)

    override fun readImage(id: String): Task<ImageSource> =
        task {
            val path = Path.removeProtocol(id)
            val bitmap = BitmapFactory.decodeStream(
                assetManager.open(
                    Path.removeFirstDash(path)
                )
            )
            it.resolve(AndroidImageSource(bitmap))
        }
}

