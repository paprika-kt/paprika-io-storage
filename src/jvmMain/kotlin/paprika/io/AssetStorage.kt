package paprika.io

import paprika.canvas.sources.ImageSource
import paprika.io.task.Task
import paprika.io.task.task
import java.io.File
import java.nio.ByteBuffer
import java.nio.channels.AsynchronousFileChannel
import java.nio.channels.CompletionHandler
import java.nio.file.Paths

class AssetStorage(private val assetDir: String) : ReadableStorage {
    override fun support(id: String): Boolean =
        StorageTypes.ASSETS.support(id)

    override fun readText(id: String): Task<String> = task {
        val path = Path.removeProtocol(id)
        it.resolve(File(Path.join(assetDir, path)).readText())
    }

    override fun readImage(id: String): Task<ImageSource> =
        task {
            val path = Path.removeProtocol(id)
            val file = File(Path.join(assetDir, path))
            it.resolve(JVMImageSource.fromPNG(file))
        }

    override fun read(id: String): Task<ByteArray> = task {
        val path = Path.removeProtocol(id)
        val channel =
            AsynchronousFileChannel.open(Paths.get(Path.join(assetDir, path)))
        channel.use { channel ->
            val buf = ByteBuffer.allocate(4096)
            channel.read(buf, 0L, buf, object : CompletionHandler<Int, ByteBuffer> {
                override fun completed(bytesRead: Int, attachment: ByteBuffer) {
                    attachment.flip()
                    val data = ByteArray(attachment.limit())
                    attachment.get(data)
                    attachment.clear()
                    it.resolve(data)
                }

                override fun failed(exception: Throwable, attachment: ByteBuffer) {
                    it.reject(exception)
                }
            })
        }
    }

    override fun has(id: String): Boolean {
        return File(Path.join(assetDir, id)).exists()
    }

}