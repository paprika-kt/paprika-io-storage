package paprika.io

import de.matthiasmann.twl.utils.PNGDecoder
import paprika.canvas.sources.ImageSource
import paprika.io.buffer.ByteArrayBuffer
import paprika.io.buffer.UByteArrayBuffer
import java.io.File

class JVMImageSource(override val bytes: UByteArrayBuffer, override val width: Int, override val height: Int) : ImageSource {


    companion object {
        fun fromPNG(file: File): JVMImageSource = file.inputStream().use {
            val decoder = PNGDecoder(it)
            val bytes = UByteArrayBuffer(4 * decoder.width * decoder.height)

            decoder.decode(bytes.buffer, decoder.width * 4, PNGDecoder.Format.RGBA)
            bytes.buffer.flip()
            JVMImageSource(bytes, decoder.width, decoder.height)
        }

    }
}