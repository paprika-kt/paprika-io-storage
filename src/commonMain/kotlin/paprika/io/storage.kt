package paprika.io

import paprika.canvas.sources.ImageSource
import paprika.io.task.Task


interface Storage : ReadableStorage {
    fun write(id: String, value: String)
}

interface ReadableStorage {
    fun support(id: String): Boolean
    fun read(id: String): Task<ByteArray> =
        NOT_SUPPORTED()

    fun readText(id: String): Task<String> =
        NOT_SUPPORTED()

    fun has(id: String): Boolean = NOT_SUPPORTED()
    fun readImage(id: String): Task<ImageSource> =
        NOT_SUPPORTED()
}

fun NOT_SUPPORTED(): Nothing = error("not supported")
fun NOT_FOUND(key: String? = null): Nothing = error(if (key != null) "not founds $key" else "not founds")

interface StorageType {
    val schema: String
}

enum class StorageTypes(override val schema: String, val default: Boolean) : StorageType {
    ASSETS("asset", true),
    SESSION("session", false),
    LOCAL("local", false),
    HTTP("http", false);

    fun support(path: String): Boolean = Path.protocol(path) == schema
}

enum class StorageFormat {
    BYTE,
    IMAGE,
    TEXT,
}