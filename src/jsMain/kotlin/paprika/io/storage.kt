package paprika.io

import org.w3c.dom.HTMLImageElement
import org.w3c.dom.get
import org.w3c.dom.set
import org.w3c.xhr.XMLHttpRequest
import paprika.io.task.Task
import paprika.io.task.task
import kotlin.browser.document
import kotlin.browser.sessionStorage

class SessionStorage : Storage {
    override fun write(id: String, value: String) {
        sessionStorage[id] = value
    }

    override fun support(id: String): Boolean {
        return id.startsWith(StorageTypes.SESSION.schema)
    }

    override fun readText(id: String): Task<String> = task {
        val result = sessionStorage[id] ?: NOT_FOUND()
        it.resolve(result)
    }

    override fun has(id: String): Boolean = sessionStorage[id] != null
}

