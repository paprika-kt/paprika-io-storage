package paprika.io

import org.w3c.dom.HTMLImageElement
import org.w3c.xhr.XMLHttpRequest
import paprika.canvas.sources.ImageSource
import paprika.io.task.task
import kotlin.browser.document

class AssetStorage(val assetDir: String) : ReadableStorage {

    override fun readText(id: String) = task<String> { trigger ->
        val req = XMLHttpRequest()
        val path = Path.removeProtocol(id)
        req.open("GET", Path.join(assetDir, path).replace("//", "/"), true)
        req.send(null)
        req.onreadystatechange = {
            if (req.readyState == XMLHttpRequest.DONE)
                trigger.resolve(req.responseText)
        }
    }

    override fun readImage(id: String) = task<ImageSource> { trigger ->
        val image = document.createElement("img") as HTMLImageElement
        val path = Path.removeProtocol(id)
        image.addEventListener("load", {
            trigger.resolve(HtmlImageSource(image))
        })
        image.src = Path.join(assetDir, path).replace("//", "/")
    }

    override fun support(id: String): Boolean =
        StorageTypes.ASSETS.support(id)

    override fun has(id: String): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}